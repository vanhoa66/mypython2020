from flask import Flask, render_template, jsonify, request
from flask_pymongo import PyMongo
import logging
import scrapy
import json

app = Flask(__name__)
log = logging.getLogger('werkzeug')
log.disabled = True
app.config['JSON_AS_ASCII'] = False
app.config["MONGO_URI"] = "mongodb://localhost:27017/pymongotruyenh"
mongo = PyMongo(app)
db_operations = mongo.db.stories


@app.route('/', methods=['POST', 'GET'])
def get_page():
    if request.method == 'POST':
        # print(request.form)  # prints ImmutableMultiDict([])
        print(request.form['url'])  # raises 400 error
        # return render_template('page.html')
    return render_template('home.html')


# @app.route('/create')
# def create():
#     new_user = {'Name': 'xyz', 'Age': 20}
#     db_operations.insert_one(new_user)
#     result = {'result': 'Created successfully'}
#     return result


@app.route('/read')
def read():
    stories = db_operations.find()
    # story = stories['chapList']
    output = [{'name': story['name']} for story in stories]
    print(output)
    links = json.dumps(output, ensure_ascii=False)
    return links


if __name__ == '__main__':
    # app.run(debug=False)
    app.run(debug=True)
