import scrapy
from flask import Flask, render_template, jsonify, request
from flask_pymongo import PyMongo
import io
import os
from PIL import Image  # https://pillow.readthedocs.io/en/4.3.x/
import requests

app = Flask(__name__)


app.config["MONGO_URI"] = "mongodb://localhost:27017/pymongotruyenh"
mongo = PyMongo(app)
db_operations = mongo.db.stories


class StorySpider(scrapy.Spider):
    name = "story"

    def start_requests(self):
        links = [
            # 'https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/chap-373'
            'https://truyentranhaudio.online/manga-slug/dai-quan-gia-la-ma-hoang/',
            'https://truyentranhaudio.online/manga-slug/solo-leveling-2/',
            'https://truyentranhaudio.online/manga-slug/vinh-hang-chi-ton/',
            'https://truyentranhaudio.online/manga-slug/vo-than-chua-te/',
            'https://truyentranhaudio.online/manga-slug/yeu-than-ky/',
            'https://truyentranhaudio.online/manga-slug/vo-dao-doc-ton/',
            'https://truyentranhaudio.online/manga-slug/ta-co-phong-rieng-thoi-tan-the/',
            'https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/',
            'https://truyentranhaudio.online/manga-slug/tu-chan-noi-chuyen-phiem-quan/',
            'https://truyentranhaudio.online/manga-slug/ta-la-dai-than-tien/',
            'https://truyentranhaudio.online/manga-slug/ngao-thi-thien-dia/',
            'https://truyentranhaudio.online/manga-slug/sieu-nang-lap-phuong/',
            'https://truyentranhaudio.online/manga-slug/dai-kiem-than/',
            'https://truyentranhaudio.online/manga-slug/dien-thoai-cua-ta-thong-tam-gioi/',
            'https://truyentranhaudio.online/manga-slug/dau-pha-thuong-khung/',
            'https://truyentranhaudio.online/manga-slug/mat-the-vi-vuong/',
            'https://truyentranhaudio.online/manga-slug/van-gioi-tien-tung/',
            'https://truyentranhaudio.online/manga-slug/nguyen-long/',
            'https://truyentranhaudio.online/manga-slug/do-thi-dinh-phong-cao-thu/',
            'https://truyentranhaudio.online/manga-slug/van-co-than-vuong/',
            'https://truyentranhaudio.online/manga-slug/ngu-linh-the-gioi/',
            'https://truyentranhaudio.online/manga-slug/tomb-raider-king/',
            'https://truyentranhaudio.online/manga-slug/toan-chuc-phap-su/',
            'https://truyentranhaudio.online/manga-slug/pham-nhan-tu-tien/',
            'https://truyentranhaudio.online/manga-slug/do-thi-kieu-hung-he-thong/',
            'https://truyentranhaudio.online/manga-slug/tien-vuong-trung-sinh/',
            'https://truyentranhaudio.online/manga-slug/nam-kiem-tien-cua-the-gioi-nu-quyen/',
            'https://truyentranhaudio.online/manga-slug/toi-cuong-van-dao-hoa/',
            'https://truyentranhaudio.online/manga-slug/gian-gioi/',
            'https://truyentranhaudio.online/manga-slug/toi-cuong-dia-bieu-hoang-kim-than/',
            'https://truyentranhaudio.online/manga-slug/tien-tru/',
            'https://truyentranhaudio.online/manga-slug/su-thuong-de-nhat-chuong-mon/',
            'https://truyentranhaudio.online/manga-slug/bach-luyen-thanh-than/',
            'https://truyentranhaudio.online/manga-slug/hanh-trinh-de-vuong/',
            'https://truyentranhaudio.online/manga-slug/dao-hoa-bao-dien/',
            'https://truyentranhaudio.online/manga-slug/vldp-audio/',
            'https://truyentranhaudio.online/manga-slug/dai-chua-te/',
            'https://truyentranhaudio.online/manga-slug/cai-the-de-ton/',
            'https://truyentranhaudio.online/manga-slug/tren-nguoi-ta-co-mot-con-rong/',
            'https://truyentranhaudio.online/manga-slug/chiem-cai-dinh-nui-lam-dai-vuong/',
            'https://truyentranhaudio.online/manga-slug/thu-nhan/',
            'https://truyentranhaudio.online/manga-slug/rich-player-nguoi-choi-khac-kim/',
            'https://truyentranhaudio.online/manga-slug/ta-tu-co-the-la-gia-tien/'
        ]
        for url in links[0:100]:
            yield scrapy.Request(url=url, callback=self.parse)
    # start_urls = [
    #     'https://truyentranhaudio.online/manga-slug/vua-thang-cap-2/'
    # ]

    def parse(self, response):
        chapList = []
        name = response.css('.breadcrumb li a::text')[1].get()
        content = response.css('.summary__content').get()
        for link in response.css('div.listing-chapters_wrap ul.main li a::attr(href)'):
            chapList.append(link.get())
        new_story = {'name': name, 'content': content, 'chapList': chapList}
        db_operations.insert_one(new_story)
        print("Done")
