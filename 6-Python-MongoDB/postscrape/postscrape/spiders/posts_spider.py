import scrapy
from flask import Flask, render_template, jsonify, request
from flask_pymongo import PyMongo
import io
import os
from PIL import Image  # https://pillow.readthedocs.io/en/4.3.x/
import requests

app = Flask(__name__)


app.config["MONGO_URI"] = "mongodb://localhost:27017/pymongotruyenh"
mongo = PyMongo(app)
db_operations = mongo.db.stories

stories = db_operations.find()
output = [{'chapList': story['chapList']} for story in stories][5]
links = output['chapList']


class PostsSpider(scrapy.Spider):
    name = "posts"

    def start_requests(self):
        # links = [
        #     'https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/chap-373'
        # ]
        # [0:20]
        for url in links[0:1000]:
            yield scrapy.Request(url=url, callback=self.parse)
    # start_urls = [
        # 'http://quotes.toscrape.com/page/1/'
        # 'https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/chap-374'
    # ]

    def parse(self, response):
        # tap = response.url.split("/")[-2].split('-')[1]
        tap = response.url.split("/")[-2].split('chap', 1)[1]
        # sub = response.url.split("/")[-2].split('-')[2]
        # tap_arr.append(chap)
        # newpath = r'C:\Program Files\arbitrary'
        new_folder = 'E:\TruyenAudio'
        newpath = os.path.join(new_folder, f"tap{tap}")
        if not os.path.exists(newpath):
            # print(newpath)
            os.makedirs(newpath)
        # else:
        #     newpath = os.makedirs(os.path.join(new_folder, f"chap-{tap}"))
        #     os.makedirs(newpath)

        imgList = []
        for img in response.css('div.text-left img::attr(src)'):
            imgList.append(img.get())
        for index, item in enumerate(imgList):
            url = item
            # image_file_path = '%s.jpg' % index
            image_file_path = os.path.join(newpath, f"{index}.jpg")
            r = requests.get(url, timeout=4.0)
            if r.status_code != requests.codes.ok:
                assert False, 'Status code error: {}.'.format(r.status_code)
            with Image.open(io.BytesIO(r.content)) as im:
                im.save(image_file_path)
