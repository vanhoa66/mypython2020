import scrapy

# url = "https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/"

# title = response.css('.post-title h3')
# print(title)


# class PostsSpider(scrapy.Spider):
#     name = "posts"

#     start_urls = [
#         'https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/'
#         # 'https://blog.scrapinghub.com/'
#     ]
#     # url = "https://truyentranhaudio.online/manga-slug/tuyet-the-vo-than/"

#     def parse(self, response):
#         title = response.css('.post-title h3')
#         print(title)


class PostsSpider(scrapy.Spider):
    name = "posts"

    start_urls = [
        'https://blog.scrapinghub.com/'
    ]

    def parse(self, response):
        for post in response.css('div.post-item'):
            yield {
                'title': post.css('.post-header h2 a::text')[0].get(),
                'date': post.css('.post-header a::text')[1].get(),
                'author': post.css('.post-header a::text')[2].get()
            }
        next_page = response.css('a.next-posts-link::attr(href)').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)
