import io
from PIL import Image  # https://pillow.readthedocs.io/en/4.3.x/
import requests  # http://docs.python-requests.org/en/master/


def download_image(url, image_file_path):
    r = requests.get(url, timeout=4.0)
    if r.status_code != requests.codes.ok:
        assert False, 'Status code error: {}.'.format(r.status_code)

    with Image.open(io.BytesIO(r.content)) as im:
        im.save(image_file_path)

    print('Image downloaded from url: {} and saved to: {}.'.format(
        url, image_file_path))


url = "https://1.bp.blogspot.com/-fNdkMwUFTXI/X7SJQZwifwI/AAAAAAAAj4c/0yrXs6JbpewjmA0PHvVFEmC_Spb4oN5cgCLcBGAsYHQ/s0/2.jpg?imgmax=0"
image_file_path = "1.jpg"
download_image(url, image_file_path)
