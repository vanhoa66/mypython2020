import csv
from matplotlib import pyplot
from collections import Counter


class DocPhim:
    def __init__(self):
        self.films = []

    def read_csv(self, file_name):
        # self.file_name = file_name
        self.films = []
        with open(file_name) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                film_name = row['Film']
                profit = row['Profit']
                year = row['Year']
                self.films.append({'film_name': film_name,
                                   'year': year,
                                   'profit': profit})

    def draw_films_by_years(self):
        # How many films in 2002, how many films in 2005, ....
        years = [int(film['year']) for film in self.films]
        counters = Counter(years)
        filtered_years = []
        number_of_films = []
        for key in counters.keys():
            filtered_years.append(key)
            number_of_films.append(counters.get(key))
        pyplot.barh(filtered_years, number_of_films,
                    color='blue',
                    label="Films / years"
                    )
        pyplot.show()

    def draw_films(self):
        film_names = [film['film_name'] for film in self.films]
        profits = [float(film['profit']) for film in self.films]
        pyplot.barh(film_names, profits, color='b',
                    label="Films / Profits", width=0.2)
        pyplot.show()
