
from PIL import Image, ImageFile
import PIL
import os
import glob
ImageFile.LOAD_TRUNCATED_IMAGES = True
# file_name = '1.jpg'
# picture = Image.open('image-1.jpg')
# quality = 60
# image = Image.open('1.jpg')
# image = image.convert('RGB')
# image.save('1.webp', 'webp',  optimize=True, quality=quality)

# , quality=quality

# Defining a Python user-defined exception

src_path = 'D:/Hoadv/Truyenvn/1/'


class Error(Exception):
    pass


def convert_image(image_path, image_file, image_type, tap_name):
    quality = 60
    # dist_path = 'E:/Truyenvn/1/'
    try:
        im = Image.open(image_path)
    except:
        print("Anh Loi roi %s " % (image_path))
        return
    im = im.convert('RGB')
    image_name = image_file.split('.')[0]
    dir_path = "E:\\Truyenvn\\Webp"
    new_dir_path = os.path.join(dir_path, tap_name)
    if not os.path.exists(new_dir_path):
        os.makedirs(new_dir_path)
    G1_Name_Formatted = ("%s" % (image_name)) + ".webp"
    file_path = os.path.join(new_dir_path, G1_Name_Formatted)
    if image_type == 'webp' or image_type == 'jpg' or image_type == 'JPG' or image_type == 'jpeg' or image_type == 'JPEG' or image_type == 'png' or image_type == 'PNG':
        im.save(file_path, 'webp',  optimize=True, quality=quality)
    # else:
    #     raise Error


# src_path = 'E:/appvivungcungcon/Python/2-Compare-Image/src/'
for src_root, src_dirs, src_files in os.walk(src_path):
    tap_name = src_root.split('/')[-1]
    for image in src_files:
        image_path = os.path.join(src_root, image)
        if image.endswith('webp'):
            convert_image(image_path=image_path, image_file=image,
                          image_type='webp', tap_name=tap_name)
        if image.endswith('jpg'):
            convert_image(image_path=image_path, image_file=image,
                          image_type='jpg', tap_name=tap_name)
        if image.endswith('JPG'):
            convert_image(image_path=os.path.join(src_root, image),
                          image_file=image, image_type='JPG', tap_name=tap_name)
        if image.endswith('jpeg'):
            convert_image(image_path=os.path.join(src_root, image),
                          image_file=image, image_type='jpeg', tap_name=tap_name)
        if image.endswith('JPEG'):
            convert_image(image_path=os.path.join(src_root, image),
                          image_file=image, image_type='JPEG', tap_name=tap_name)
        if image.endswith('png'):
            convert_image(image_path=os.path.join(src_root, image),
                          image_file=image, image_type='png', tap_name=tap_name)
        elif image.endswith('PNG'):
            convert_image(image_path=os.path.join(src_root, image),
                          image_file=image, image_type='PNG', tap_name=tap_name)
        # else:
        #     raise Error
    # dest = f"{tap_name}_{src_file}"
    # print(dest)
    # for root, dirs, files in os.walk(os.path.join(src_root, src_file)):
    #     for file in files:
    #

    # for image in images:
    # if image.endswith('jpg'):
    #     convert_image(image, image_type='jpg')
    # if image.endswith('JPG'):
    #     convert_image(image, image_type='JPG')
    # if image.endswith('jpeg'):
    #     convert_image(image, image_type='jpeg')
    # if image.endswith('JPEG'):
    #     convert_image(image, image_type='JPEG')
    # if image.endswith('png'):
    #     convert_image(image, image_type='png')
    # elif image.endswith('PNG'):
    #     convert_image(image, image_type='PNG')
    # else:
    #     raise Error
    # image = Image.open(os.path.join(src_root, src_file))
    # image = image.convert('RGB')
    # image.save('1.webp', 'webp',  optimize=True, quality=quality)
