# from os import listdir, rename, makedirs
# from os.path import isfile, join, exists
# import subprocess
# from tqdm import tqdm
from PIL import Image, ImageFile
import PIL
import os
import glob
import subprocess
ImageFile.LOAD_TRUNCATED_IMAGES = True

src_path = "E:/appvivungcungcon/Python/Epub-to-Mobi-Converter-master/Epub-to-Mobi-Converter-master/ebooks/"
for src_root, src_dir, src_files in os.walk(src_path):
    for src_file in src_files:
        file_path = os.path.join(src_root, src_file)
        try:
            subprocess.call(["ebook-convert", file_path, src_path])
        except Exception as e:
            print(e)

# mypath = "E:/appvivungcungcon/Python/Epub-to-Mobi-Converter-master/Epub-to-Mobi-Converter-master/1.epub"
# mypath_converted = "E:/appvivungcungcon/Python/Epub-to-Mobi-Converter-master/Epub-to-Mobi-Converter-master/1.azw3"
# subprocess.run(["ebook-convert", mypath, mypath_converted])


# from os import listdir, rename, makedirs
# from os.path import isfile, join, exists
# import subprocess
# from tqdm import tqdm

# # list of extensions that needs to be ignored.
# ignored_extensions = ["pdf"]

# # Set path to your Downloads Dir"
# machine_path = "E:/appvivungcungcon/Python/Epub-to-Mobi-Converter-master/"


# # Check all Directories are present - if not, make them for user
# # here all the downloaded files are kept
# mypath = machine_path + "ebooks/"

# if not exists(mypath):
#     makedirs(mypath)

# # path where converted files are stored
# mypath_converted = machine_path + "ebooks/kindle/"
# if not exists(mypath_converted):
#     makedirs(mypath_converted)

# # path where processed files will be moved to, clearing the downloaded folder
# mypath_processed = machine_path + "ebooks/processed/"
# if not exists(mypath_processed):
#     makedirs(mypath_processed)
# #
# raw_files = [f for f in listdir(mypath) if isfile(join(mypath, f))]
# converted_files = [f for f in listdir(
#     mypath_converted) if isfile(join(mypath_converted, f))]
# #
# # return file extension. pdf or epub or mobi


# def get_file_extension(f):
#     return f.split(".")[-1]
# # return name of file to be kept after conversion.
# # we are just changing the extension. azw3 here.


# def get_final_filename(f):
#     f = f.split(".")
#     filename = ".".join(f[0:-1])
#     processed_file_name = filename+".azw3"
#     # processed_file_name = filename+".mobi"
#     return processed_file_name
# #


# def convert_files():
#     for f in tqdm(raw_files):
#         final_file_name = get_final_filename(f)
#         extension = get_file_extension(f)
#         if final_file_name not in converted_files and extension not in ignored_extensions:
#             print("Converting : "+f)
#             try:
#                 subprocess.call(["ebook-convert", mypath+f,
#                                  mypath_converted+final_file_name])
#                 s = rename(mypath+f, mypath_processed+f)
#                 print(s)
#             except Exception as e:
#                 print(e)
#         else:
#             print("Already exists : "+final_file_name)


# # Main Driver
# if __name__ == "__main__":
#     # convert any suitable files not already converted
#     srcfiles = ([f for f in listdir(mypath) if isfile(join(mypath, f))])
#     if srcfiles:
#         convert_files()
#     else:
#         print("No files to convert")
